'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _ResponseBody = _interopRequireDefault(require("./ResponseBody"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ERROR_TYPE = {
  MongooseError: 'MongooseError',
  CastError: 'CastError',
  DisconnectedError: 'DisconnectedError',
  DivergentArrayError: 'DivergentArrayError',
  DocumentNotFoundError: 'DocumentNotFoundError',
  ValidatorError: 'ValidatorError',
  ValidationError: 'ValidationError',
  MissingSchemaError: 'MissingSchemaError',
  ObjectExpectedError: 'ObjectExpectedError',
  ObjectParameterError: 'ObjectParameterError',
  OverwriteModelError: 'OverwriteModelError',
  ParallelSaveError: 'ParallelSaveError',
  StrictModeError: 'StrictModeError',
  VersionError: 'VersionError',
  RuntimeError: 'RuntimeError'
};
var STATUS_CODE_MAP = {
  MongooseError: 500,
  CastError: 422,
  DisconnectedError: 500,
  DivergentArrayError: 409,
  DocumentNotFoundError: 404,
  ValidatorError: 422,
  ValidationError: 422,
  MissingSchemaError: 500,
  ObjectExpectedError: 422,
  ObjectParameterError: 500,
  OverwriteModelError: 500,
  ParallelSaveError: 409,
  StrictModeError: 500,
  VersionError: 409,
  RuntimeError: 500
};
var ERROR_NAME = 'MongoError';
var ERROR_CLASSIFICATION = 'DATABASE_ERROR';

class MongoError extends Error {
  constructor(error) {
    var {
      _isMongoError,
      message,
      msg,
      name,
      type,
      statusCode,
      errors
    } = error;

    var _msg = msg || message;

    super(_msg);
    Error.captureStackTrace(this, MongoError);
    this._isMongoError = true;
    this.name = ERROR_NAME;
    this.classification = ERROR_CLASSIFICATION;
    this.message = _msg;
    this.msg = _msg;
    this.statusCode = _isMongoError && statusCode || STATUS_CODE_MAP[name] || 500;
    this.type = _isMongoError && type || name;
    this.errors = errors;
    (0, _autoBind.default)(this);
  }

  getResponseBody() {
    var {
      statusCode,
      message
    } = this;
    return new _ResponseBody.default(statusCode, message, undefined, this);
  }

  static get STATUS_CODE_MAP() {
    return STATUS_CODE_MAP;
  }

  static get ERROR_TYPE() {
    return ERROR_TYPE;
  }

}

exports.default = MongoError;