'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _MongoError = _interopRequireDefault(require("./MongoError"));

var _autoBind = _interopRequireDefault(require("auto-bind"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var {
  ERROR_TYPE
} = _MongoError.default;

class MongoModel {
  constructor(modelName, Schema) {
    this.ModelName = modelName;
    this.Schema = Schema;
    this.MongooseModel = _mongoose.default.model(modelName, Schema);
    (0, _autoBind.default)(this);
  }

  _execQuery(query) {
    var _this = this;

    return _asyncToGenerator(function* () {
      try {
        var {
          Schema
        } = _this;
        var {
          _options = {}
        } = Schema;
        var {
          projection,
          populate,
          sort
        } = _options;
        var queryResult = sort ? query.sort(sort) : query;
        queryResult = projection ? queryResult.select(projection) : queryResult;
        queryResult = populate ? queryResult.populate(populate) : queryResult;
        return yield queryResult.exec();
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  _findRaw(query, projection) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      try {
        var docs = yield _this2.MongooseModel.find(query || {}, projection || null).exec();

        if (!docs.length) {
          throw {
            message: 'Documents Not Found',
            name: ERROR_TYPE.DocumentNotFoundError,
            errors: {
              query,
              projection
            }
          };
        }

        return docs;
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  _findOneRaw(query, projection) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      try {
        var doc = yield _this3.MongooseModel.findOne(query || {}, projection || null).exec();

        if (!doc) {
          throw {
            message: 'Document Not Found',
            name: ERROR_TYPE.DocumentNotFoundError,
            errors: {
              query,
              projection
            }
          };
        }

        return doc;
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  _find(query) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      try {
        var mongoQuery = _this4.MongooseModel.find(query || {}).lean();

        var docs = yield _this4._execQuery(mongoQuery);

        if (!docs.length) {
          throw {
            message: 'Documents Not Found',
            name: ERROR_TYPE.DocumentNotFoundError,
            errors: {
              query
            }
          };
        }

        return docs;
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  _findOne(query) {
    var _this5 = this;

    return _asyncToGenerator(function* () {
      try {
        var mongoQuery = _this5.MongooseModel.findOne(query || {}).lean();

        var doc = yield _this5._execQuery(mongoQuery);

        if (!doc) {
          throw {
            message: 'Document Not Found',
            name: ERROR_TYPE.DocumentNotFoundError,
            errors: {
              query
            }
          };
        }

        return doc;
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  _aggregate(query) {
    var _this6 = this;

    return _asyncToGenerator(function* () {
      try {
        return yield _this6.MongooseModel.aggregate(query);
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  list(query) {
    var _this7 = this;

    return _asyncToGenerator(function* () {
      var {
        Schema
      } = _this7;
      var {
        _options = {}
      } = Schema;
      var {
        filterProps = []
      } = _options;
      var params = {};
      filterProps.forEach(key => {
        var value = query[key];

        if (value !== undefined) {
          var valueArray = value.toString().split(',');
          params[key] = valueArray.length > 1 ? {
            $in: valueArray
          } : value;
        }
      });
      return yield _this7._find(params);
    })();
  }

  findById(id) {
    var _this8 = this;

    return _asyncToGenerator(function* () {
      var idArray = id.toString().split(',');
      var findFunc = idArray.length === 1 ? _this8._findOne : _this8._find;
      var query = {
        _id: idArray.length === 1 ? idArray[0] : {
          $in: idArray
        }
      };
      return yield findFunc(query);
    })();
  }

  create(attrs) {
    var _this9 = this;

    return _asyncToGenerator(function* () {
      try {
        return yield _this9.MongooseModel.create(attrs);
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

  update() {
    var _arguments = arguments,
        _this10 = this;

    return _asyncToGenerator(function* () {
      var query = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : {};
      var update = _arguments.length > 1 && _arguments[1] !== undefined ? _arguments[1] : {};
      var options = _arguments.length > 2 && _arguments[2] !== undefined ? _arguments[2] : {};
      var alwaysArray = _arguments.length > 3 && _arguments[3] !== undefined ? _arguments[3] : false;
      var ok;
      var n;

      try {
        var updateResult = yield _this10.MongooseModel.update(query, {
          $set: update
        }, options);
        ok = updateResult.ok;
        n = updateResult.n;

        if (!ok) {
          throw {
            message: 'Error Updating Document',
            name: ERROR_TYPE.RuntimeError,
            errors: {
              query,
              update,
              options
            }
          };
        }

        if (!n) {
          throw {
            message: 'No Document found for Update',
            name: ERROR_TYPE.DocumentNotFoundError,
            errors: {
              query
            }
          };
        }
      } catch (e) {
        throw new _MongoError.default(e);
      }

      var docs = yield _this10._find(query);

      if (n === 1 && !alwaysArray) {
        return docs[0];
      }

      return docs;
    })();
  }

  remove() {
    var _arguments2 = arguments,
        _this11 = this;

    return _asyncToGenerator(function* () {
      var query = _arguments2.length > 0 && _arguments2[0] !== undefined ? _arguments2[0] : {};
      var options = _arguments2.length > 1 && _arguments2[1] !== undefined ? _arguments2[1] : {};

      try {
        var removeResult = yield _this11.MongooseModel.remove(query, options);
        var {
          ok,
          n
        } = removeResult;

        if (!ok) {
          throw {
            message: 'Error Removing Document',
            name: 'RuntimeError',
            errors: {
              query,
              options
            }
          };
        }

        if (!n) {
          throw {
            message: 'No Document found for Removing',
            name: 'DocumentNotFoundError',
            errors: {
              query
            }
          };
        }

        return removeResult;
      } catch (e) {
        throw new _MongoError.default(e);
      }
    })();
  }

}

exports.default = MongoModel;