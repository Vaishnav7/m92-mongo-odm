'use strict'

import mongoose from 'mongoose'
import MongoError from './MongoError'
import autoBind from 'auto-bind'

const { ERROR_TYPE } = MongoError

export default class MongoModel {
  constructor (modelName, Schema) {
    this.ModelName = modelName
    this.Schema = Schema
    this.MongooseModel = mongoose.model(modelName, Schema)

    autoBind(this)
  }

  async _execQuery (query) {
    try {
      const { Schema } = this
      const { _options = {} } = Schema
      const { projection, populate, sort } = _options

      let queryResult = sort
        ? query.sort(sort)
        : query

      queryResult = projection
        ? queryResult.select(projection)
        : queryResult

      queryResult = populate
        ? queryResult.populate(populate)
        : queryResult

      return await queryResult.exec()
    } catch (e) { throw new MongoError(e) }
  }

  async _findRaw (query, projection) {
    try {
      const docs = await this.MongooseModel.find((query || {}), (projection || null)).exec()

      if (!docs.length) {
        throw {
          message: 'Documents Not Found',
          name: ERROR_TYPE.DocumentNotFoundError,
          errors: { query, projection }
        }
      }

      return docs
    } catch (e) { throw new MongoError(e) }
  }

  async _findOneRaw (query, projection) {
    try {
      const doc = await this.MongooseModel.findOne((query || {}), (projection || null)).exec()

      if (!doc) {
        throw {
          message: 'Document Not Found',
          name: ERROR_TYPE.DocumentNotFoundError,
          errors: { query, projection }
        }
      }

      return doc
    } catch (e) { throw new MongoError(e) }
  }

  async _find (query) {
    try {
      const mongoQuery = this.MongooseModel.find(query || {}).lean()
      const docs = await this._execQuery(mongoQuery)

      if (!docs.length) {
        throw {
          message: 'Documents Not Found',
          name: ERROR_TYPE.DocumentNotFoundError,
          errors: { query }
        }
      }

      return docs
    } catch (e) { throw new MongoError(e) }
  }

  async _findOne (query) {
    try {
      const mongoQuery = this.MongooseModel.findOne(query || {}).lean()
      const doc = await this._execQuery(mongoQuery)

      if (!doc) {
        throw {
          message: 'Document Not Found',
          name: ERROR_TYPE.DocumentNotFoundError,
          errors: { query }
        }
      }

      return doc
    } catch (e) { throw new MongoError(e) }
  }

  async _aggregate (query) {
    try {
      return await this.MongooseModel.aggregate(query)
    } catch (e) { throw new MongoError(e) }
  }

  async list (query) {
    const { Schema } = this
    const { _options = {} } = Schema
    const { filterProps = [] } = _options
    const params = {}

    filterProps.forEach(key => {
      let value = query[key]

      if (value !== undefined) {
        const valueArray = value.toString().split(',')

        params[key] = valueArray.length > 1
        ? { $in: valueArray }
        : value
      }
    })

    return await this._find(params)
  }

  async findById (id) {
    const idArray = id.toString().split(',')
    const findFunc = idArray.length === 1 ? this._findOne : this._find
    const query = {
      _id: idArray.length === 1 ? idArray[0] : { $in: idArray }
    }

    return await findFunc(query)
  }

  async create (attrs) {
    try {
      return await this.MongooseModel.create(attrs)
    } catch (e) { throw new MongoError(e) }
  }

  async update (query = {}, update = {}, options = {}, alwaysArray = false) {
    let ok
    let n

    try {
      const updateResult = await this.MongooseModel.update(query, { $set: update }, options)
      ok = updateResult.ok
      n = updateResult.n

      if (!ok) {
        throw {
          message: 'Error Updating Document',
          name: ERROR_TYPE.RuntimeError,
          errors: { query, update, options }
        }
      }

      if (!n) {
        throw {
          message: 'No Document found for Update',
          name: ERROR_TYPE.DocumentNotFoundError,
          errors: { query }
        }
      }
    } catch (e) { throw new MongoError(e) }

    const docs = await this._find(query)
    if (n === 1 && !alwaysArray) { return docs[0] }
    return docs
  }

  async remove (query = {}, options = {}) {
    try {
      const removeResult = await this.MongooseModel.remove(query, options)
      const { ok, n } = removeResult

      if (!ok) {
        throw {
          message: 'Error Removing Document',
          name: 'RuntimeError',
          errors: { query, options }
        }
      }

      if (!n) {
        throw {
          message: 'No Document found for Removing',
          name: 'DocumentNotFoundError',
          errors: { query }
        }
      }

      return removeResult
    } catch (e) { throw new MongoError(e) }
  }
}
