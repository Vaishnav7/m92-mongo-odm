'use strict'

import * as mongooseExports from 'mongoose'
import mongoSchemaWrapper from './mongoSchemaWrapper'
import MongoModel from './MongoModel'
import MongoError from './MongoError'

const { default: mongoose } = mongooseExports
const exportProps = {
  ...mongooseExports,
  mongoose,
  mongoSchemaWrapper,
  MongoModel,
  MongoError
}

module.exports = exportProps
